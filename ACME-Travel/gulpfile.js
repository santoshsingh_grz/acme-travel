﻿var gulp = require('gulp');

// Include Our Plugins
var sass = require('gulp-sass')(require('sass'));


gulp.task('sass', async function () {
    return gulp.src('assets/scss/styles.scss')
        .pipe(sass()) // Converts Sass to CSS with gulp-sass
        .pipe(gulp.dest('assets/css'))
});

// Watch Files For Changes
gulp.task('watch', function () {
    gulp.watch('assets/scss/**/*.scss', gulp.series(['sass']));
    //gulp.watch('assets/media/**/*', ['optimizeImages']);
});